package in.nfnlabs.masterdetail.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
  */

    int id;
    String content;
    String Desc;

    public List<DummyContent> getDummyContents() {
        return dummyContents;
    }

    public DummyContent(int id, String content, String desc) {
        this.id = id;
        this.content = content;
        this.Desc = desc;
    }

    public void setDummyContents(List<DummyContent> dummyContents) {
        this.dummyContents = dummyContents;
    }

    List<DummyContent> dummyContents;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }
}
