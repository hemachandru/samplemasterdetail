package in.nfnlabs.masterdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.nfnlabs.masterdetail.dummy.DummyContent;

import java.util.ArrayList;
import java.util.List;


public class ItemListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private Fragment fragment;
    List<DummyContent> value=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
        }

        View recyclerView = findViewById(R.id.item_list);
        assert recyclerView != null;
        populatedata();
        setupRecyclerView((RecyclerView) recyclerView);
    }

    private void populatedata() {
        value.add(new DummyContent(1,"hai","dummy"));
        value.add(new DummyContent(2,"hai","dummy"));
        value.add(new DummyContent(3,"hai","dummy"));
        value.add(new DummyContent(4,"hai","dummy"));
        value.add(new DummyContent(5,"hai","dummy"));
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this,value, mTwoPane));
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ItemListActivity mParentActivity;
        private final List<DummyContent> mValues;
        private final boolean mTwoPane;


        private void selectVitalFragment(int id) {
            //creating fragment object

            switch (id) {
                case 1: {
                    fragment = new VisitDetailsFragment();
                }
                break;
                case 2: {
                    fragment = new VisitPrescriptionFragment();
                }
                break;
                case 3: {

                    fragment = new VisitInvestigationFragment();
                }
                break;
                case 4: {
                    fragment = new VisitVitalsFragment();
                }
                break;
                case 5: {

                    fragment = new VisitInvestigationFragment();
                }
                break;
                default: {
                    fragment = new VisitDetailsFragment();
                }
                break;

            }

            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.item_detail_container, fragment);
                ft.commit();
            }
        }

        SimpleItemRecyclerViewAdapter(ItemListActivity parent,
                                      List<DummyContent> items,
                                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
//            holder.mIdView.setText(mValues.get(position).getId());
//            holder.mContentView.setText(mValues.get(position).getContent());
//
//            holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        selectVitalFragment(mValues.get(position).getId());
                    } else {
                        Context context = mParentActivity.getBaseContext();
                        Intent intent = new Intent(context, ItemDetailActivity.class);
                        intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, mValues.get(position).getId());

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mIdView = (TextView) view.findViewById(R.id.id_text);
                mContentView = (TextView) view.findViewById(R.id.content);
            }
        }
    }
}
